<?php

namespace app\admin\model\course;

use think\Model;


class ScoreResult extends Model
{

    

    

    // 表名
    protected $name = 'score_result';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;
    protected $resultSetType = 'collection';
    // 追加属性
    protected $append = [

    ];
    

    //获取课程的评分标准
    function getResultList($pid,$unit =0 ){
        //unit =3 是团队的质量评分，=0是个人体能
        $ret = $this->where(['pid' => $pid,'unit'=>$unit])->order('level asc')->select()->toArray();
        return $ret;
    }






}
