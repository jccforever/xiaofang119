<?php

namespace app\admin\model\user;

use think\Model;


class Level extends Model
{

    

    

    // 表名
    protected $table = 'fa_user_level';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;
    protected $resultSetType = 'collection';

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];

    public function getLevelData()
    {
        $data=[];
        $list  = self::where(1)->select()->toArray();
        foreach($list as $k=>$val){
            $data[$val['id']] = $val['name'] ;
        }
        return $data;
    }

    







}
