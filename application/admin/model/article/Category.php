<?php

namespace app\admin\model\article;

use think\Model;
use fast\Tree;


class Category extends Model
{

    

    

    // 表名
    protected $name = 'web_category';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];


    //部门列表。
    //用于添加权限账号页面使用，显示部门列表，不包含班级(这里的depart_id处理过)
    //参数
    public static function getCategoryList( )
    {


        $ruleList = collection(self::where(1)->field('pid,name,category_id as id')->order('category_id asc')->select())->toArray();

        //获取实例
        $tree = Tree::instance();
        //初始化
        /*
             * @param array  $arr     2维数组，例如：
             * @param string $pidname 父字段名称
             * @param string $nbsp    空格占位符
        */
        $tree->init($ruleList , 'pid', '&nbsp');

        //获取树状数组
        /**
         *
         * 获取树状数组
         * @param string $myid       要查询的ID
         * @param string $itemprefix 前缀
         * @return array
         */


        $treeArr = $tree->getTreeArray(0, '&nbsp');

        //将getTreeArray的结果返回为二维数组
        /**
         * 将getTreeArray的结果返回为二维数组
         * @param array  $data
         * @param string $field
         * @return array
         */
        $list = $tree->getTreeList($treeArr, 'name');


        return $list;
    }










}
