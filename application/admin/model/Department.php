<?php

namespace app\admin\model;

use think\Model;
use fast\Tree;


class Department extends Model
{

    

    

    // 表名
    protected $name = 'department';
    protected $primarykey = 'depart_id';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    protected static function init()
    {
        self::afterInsert(function ($row) {
            $pk = $row->getPk();
            $row->getQuery()->where($pk, $row[$pk])->update(['weigh' => $row[$pk]]);
        });
    }

    public static function getTreeList($depart_id=0,$selected = [])
    {

        $ruleList = collection(self::where(1)->order('weigh asc,depart_id desc')->select())->toArray();
        foreach($ruleList as $k=>$val){
            $ruleList[$k]['id'] = $val['depart_id'] ;
            unset($ruleList[$k]['depart_id']);
        }
        $nodeList = [];
        Tree::instance()->init($ruleList);
        $ruleList = Tree::instance()->getTreeList(Tree::instance()->getTreeArray($depart_id >0 ? $depart_id :0), 'title' ,false);
        $hasChildrens = [];
        foreach ($ruleList as $k => $v)
        {
            if ($v['haschild'])
                $hasChildrens[] = $v['id'];
        }
        foreach ($ruleList as $k => $v) {
            $state = array('selected' => in_array($v['id'], $selected) && !in_array($v['id'], $hasChildrens));
            $nodeList[] = array('id' => $v['id'], 'parent' => $v['pid'] ? $v['pid'] : '#', 'text' => __($v['title']), 'type' => 'menu', 'state' => $state);
        }

        if($depart_id >0){ //将自身作为tree的起点
            $self_info =self::get($depart_id);
            array_unshift( $nodeList,  array('id' => $self_info['depart_id'], 'parent' =>'#', 'text' => __($self_info['title']), 'type' => 'menu', 'state' => $state)); //在数组开头插入元素
        }

        return $nodeList;
    }

    //查询部门的上级
    function getParentDepart($depart_id){

         $info = self::get($depart_id);
         if($info['level'] >2){
             $info = self::get($info['pid']);
             return $info;
         }
        return  [];
    }

    //获取部门名称
    function getDepartName($depart_id){
        if($depart_id){
            $info = self::get($depart_id);
            return  $info['title'];
        }else{
            return '--';
        }

    }

    //部门列表。
    //用于添加权限账号页面使用，显示部门列表，不包含班级(这里的depart_id处理过)
    //参数
    public static function getDepartList( $auth ,$depart_id=0 ,$class =0)
    {


        if($class==1){ //当class=1 时，需要查询班级
            $class = 0;
        }else{
            $class =5;
        }
        $ruleList = collection(self::where('level','<>',$class)->order('weigh asc,depart_id desc')->select())->toArray();

        foreach($ruleList as $k=>$val){
            $ruleList[$k]['id'] = $val['depart_id'] ;
            unset($ruleList[$k]['depart_id']);
        }
        $nodeList = [];
        Tree::instance()->init($ruleList);
        $ruleList = Tree::instance()->getTreeList(Tree::instance()->getTreeArray( $depart_id>0 ?$depart_id :0 ), 'title' ,false);
        $hasChildrens = [];

        //部门级别
        $level = 0 ; //默认支队
        if($depart_id>0){
            $info = self::get($depart_id);
            $level = $info['level'] ;
        }

        //前缀
        $str1 = '&nbsp;'.'└&nbsp;' ;
        $str2 = '&nbsp;&nbsp;&nbsp;'.'└&nbsp;' ;

        if($level==2){
            $str1 = '' ;
            $str2 = '&nbsp;'.'└&nbsp;' ;
        }elseif($level ==3){
            $str1 = '' ;
            $str2 = '&nbsp;'.'└&nbsp;' ;
        }

        foreach ($ruleList as $k => $v)
        {
            if ($v['haschild'])
                $hasChildrens[] = $v['id'];
        }


        foreach ($ruleList as $k => $v) {



            if($v['level'] == 3)
            {
                $v['title'] = $str1 . $v['title'];
            }
            elseif($v['level'] == 4)
            {
                $v['title'] =  $str2 . $v['title'];
            }
            elseif($v['level'] == 5)
            {
                $v['title'] = '&nbsp;&nbsp;'. $str2 . $v['title'];
            }

            $nodeList[$v['id']] =  $v['title'];
        }
        return $nodeList;
    }






}
