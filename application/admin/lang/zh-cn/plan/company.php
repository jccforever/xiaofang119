<?php

return [
    'Id'          => '表id',
    'Cid'          => 'ID',
    'Title'          => '单位名称',
    'Mobile'          => '联系电话',
    'Consignee'          => '联系人',
    'Sort'          => '排序',
    'Zd_name3'=>'所属单位',
    'Address'=>'所属路段',
    'Department' =>'上传单位',
    'Create_time' => '添加时间',
    'Unit'        => '单位属性',
    'Doc'         => '预案文件',
    'Content'         => '预案内容',
];
