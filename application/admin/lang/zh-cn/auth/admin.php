<?php

return [
    'Type'                                     => '账号类型',
    'Group'                                     => '所属部门',
    'Loginfailure'                              => '登录失败次数',
    'Login time'                                => '最后登录',
    'Create time'                                => '创建时间',
    'The parent group exceeds permission limit' => '父组别超出权限范围',
    'Please input correct username'             => '用户名只能由3-12位数字、字母、下划线组合',
    'Please input correct password'             => '密码长度必须在6-16位之间，不能包含空格',
];
