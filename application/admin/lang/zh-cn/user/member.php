<?php

return [
    'User_id'     => 'ID',
    'Create_time' => '注册时间',
    'Mobile'      => '电话',
    'Realname'    => '姓名',
    'Card'        => '身份证',
    'Sex'         => '性别',
    'Zd_id2'      => '支队',
    'Zd_id3'      => '大队',
    'Zd_id4'      => '中队',
    'Zd_id5'      => '班级',
    'Age'         => '年龄',
    'Birthday'    => '出生日期',
    'Sn'          => '证件编号',
    'Level'       => '人员类别',
    'Photo'       => '证件照',
    'Join_time'   => '入伍时间',
    'Depart'      => '单位',
];
