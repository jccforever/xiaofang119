<?php

return [
    'Id'           => '表id',
    'Create_time'  => '注册时间',
    'User_id'      => 'ID',
    'Lesson_id'    => '考核科目',
    'Unit'         => '科目类型',
    'Num'          => '次数',
    'Status'       => '2是缺考',
    'Action_score' => '质量得分',
    'Action_level' => '质量等级',
    'Time_score'   => '时间得分',
    'Time_level'   => '时间等级',
    'Week'         => '每月第几周5代表月考',
    'Year'         => '年份'
];
