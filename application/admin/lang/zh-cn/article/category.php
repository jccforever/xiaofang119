<?php

return [
    'Category_id' => '序号',
    'Name'        => '标题',
    'Sequence'    => '排序',
    'Is_show'     => '状态',
    'Is_nav'      => '导航',
    'Type'        => '类型',
    'Limit'       => '限制',
    'Pid'         => '上级'
];
