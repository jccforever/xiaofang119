<?php

return [
    'Article_id'  => '序号',
    'Title'       => '标题',
    'Category_id' => '分类',
    'Content'     => '内容',
    'Create_time' => '活动时间',
    'Admin_id'    => '上传人员',
    'Thumb'       => '缩略图',
    'Description' => '描述',
    'On_time'     => '创建时间',
    'Work_time'   => '工作时间',
    'Leader'      => '指挥员'
];
