<?php

namespace app\admin\controller\plan;

use app\common\controller\Backend;

use think\Db;

/**
 * 预案单位
 *
 * @icon fa fa-circle-o
 */
class Company extends Backend
{
    
    /**
     * Company模型对象
     * @var \app\admin\model\plan\Company
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\plan\Company;

        //账号等级
        $this->group = $this->auth->getGroups();
        $zdlist3 = [];
        if($this->group[0]['group_id'] <= 2){ //总账号
            $zdlist3= Db::name('department')->where('level',3)->select();
        }
        $this->view->assign('zdlist3',$zdlist3);

        //全景附件数量
        $filenum =[1,2,3,4,5,6,7,8,9,10];
        $this->view->assign('filenum',$filenum);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();


            $total = $this->model
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            $DepartModel = model('Department');
            foreach ($list as $k=>$row) {
                $list[$k]['in_depart'] = $DepartModel->getDepartName($row['zd_id3']);
                $list[$k]['unit'] = Db::name('unit')->where('id',$row['unit'])->value('name');
                $list[$k]['up_depart'] = $DepartModel->getDepartName($row['author']);

            }


            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {


        if ($this->request->isPost()) {
            //$this->token();
            $params = $this->request->post("row/a", [], 'trim');

            if ($params) {

                //附件数组
                $name= $this->request->post("name/a", [], 'trim');
                $file = $this->request->post("file/a", [], 'trim');
                $url = $this->request->post("qj/a", [], 'trim');

                //新增中队权限-----2020-07-24------
                $admin = $this->auth->getUserInfo() ;
                $params['zd_id4'] = 0 ;//默认消防站0
                $last_cid = Db::name('company')->where(1)->order('id desc')->value('cid');//当前序号
                $params['cid'] = $last_cid + 1;

                if( $this->group[0]['group_id']  == 4){ //消防站
                    $params['zd_id4'] = $params['zd_id3'] ;
                    $params['zd_id3'] = Db::name('department')->where('depart_id',$params['zd_id4'])->value('pid') ;
                    $params['zd_id2'] = Db::name('department')->where('depart_id',$params['zd_id3'])->value('pid') ;

                }else if( $this->group[0]['group_id'] == 3 ){ //大队

                    $params['zd_id2'] = Db::name('department')->where('depart_id',$params['zd_id3'])->value('pid') ;
                }
                //添加作者
                $params['author'] = $admin['department_id'] ?$admin['department_id'] :0;

                //新增中队权限---限-----2020-07-24--------------

                $params['create_time'] = time();

                try {

                    $result = $this->model->create($params);

                    if ($result){
                        foreach($name as $k=>$val){
                            if(!empty($val)){
                                Db::name('file')->insert([
                                    'cid'=>$result['cid']  ,'file'=>$file[$k],'name'=>$val,'url'=>$url[$k],'order'=>$k
                                ]);//这里的cid关联的是单位的序号cid非id
                            }
                        }
                    }

                } catch (Exception $e) {
                    $this->error($e->getMessage());
                }
                $this->success();

            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        //所属单位
        $admin = $this->auth->getUserInfo() ;
        if($this->group[0]['group_id'] == 1){ //总账号 显示当前+下一级
            $map['level'] =$this->group[0]['group_id']  + 2;
            $zdlist = Db::name('department')->where($map)->order('depart_id asc')->select();

        }else{
            $map['level']=3; //不要班
            $map['id'] = $admin['department_id'];
            $zdlist = Db::name('department')->where($map)->order('depart_id asc')->select();
           // $this->assign('zd_id3',$admin['department_id']);

        }
        $this->view->assign('zdlist',$zdlist);
        $this->view->assign('zd_id3',$admin['department_id']);
        //单位属性
        $unit = Db::name('unit')->where(1)->order('id asc')->select();
        $this->view->assign('unit',$unit);

        return $this->view->fetch();
    }

    /**
     * 编辑
     * @param null $ids
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            //$this->token();
            $params = $this->request->post("row/a", [], 'trim');
            //附件数组
            $name= $this->request->post("name/a", [], 'trim');
            $file = $this->request->post("file/a", [], 'trim');
            $url = $this->request->post("qj/a", [], 'trim');

            if( $this->group[0]['group_id']  == 4){ //消防站
                $params['zd_id4'] = $params['zd_id3'] ;
                $params['zd_id3'] = Db::name('department')->where('depart_id',$params['zd_id4'])->value('pid') ;
                $params['zd_id2'] = Db::name('department')->where('depart_id',$params['zd_id3'])->value('pid') ;

            }else if( $this->group[0]['group_id'] == 3 ){ //大队

                $params['zd_id2'] = Db::name('department')->where('depart_id',$params['zd_id3'])->value('pid') ;
            }

            try {
                $result = $row->allowField(true)->save($params);
                if ($result !== false) {
                    //更新附件

                        Db::name('file')->where('cid',$params['cid'])->delete();
                        foreach($name as $k=>$val){
                            if(!empty($val)){
                                Db::name('file')->insert([
                                    'cid'=>$params['cid']  ,'file'=>$file[$k],'name'=>$val,'url'=>$url[$k],'order'=>$k
                                ]);//这里的cid关联的是单位的序号cid非id

                            }
                        }


                    $this->success();
                } else {
                    $this->error($row->getError());
                }
            } catch (\think\exception\PDOException $e) {
                $this->error($e->getMessage());
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        //附件
        $array =[];
        $file = Db::name('file')->where('cid',$row['cid'])->order('id asc')->select();
         foreach($file as $k=>$val){
             if($val){
                 $array[$val['order']]['file'] = $val['file'];
                 $array[$val['order']]['name'] = $val['name'];
                 $array[$val['order']]['url'] = $val['url'];
             }
         }
         $this->view->assign('file',$array);

        //所属单位
        $admin = $this->auth->getUserInfo() ;
        if($this->group[0]['group_id'] == 1){ //总账号 显示当前+下一级
            $map['level'] =$this->group[0]['group_id']  + 2;
            $zdlist = Db::name('department')->where($map)->order('depart_id asc')->select();

        }else{
            $map['level']=3; //不要班
            $map['id'] = $admin['department_id'];
            $zdlist = Db::name('department')->where($map)->order('depart_id asc')->select();
            // $this->assign('zd_id3',$admin['department_id']);

        }
        $this->view->assign('zdlist',$zdlist);
        $this->view->assign('zd_id3',$row['zd_id3']);
        //单位属性
        $unit = Db::name('unit')->where(1)->order('id asc')->select();
        $this->view->assign('unit',$unit);

        $this->view->assign('row',$row);
        return $this->view->fetch();
    }

}
