<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use fast\Tree;

/**
 * 机构列管理
 *
 * @icon fa fa-circle-o
 */
class Department extends Backend
{
    
    /**
     * Department模型对象
     * @var \app\admin\model\Department
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Department');

        //add 、edit下拉列表数据
        $groupdata =  \app\admin\model\Department::getDepartList($this->auth->isSuperAdmin() , $this->auth->department_id);
  
        if( !$this->auth->isSuperAdmin()){
            $self_info =\app\admin\model\Department::get($this->auth->department_id);

            //数组首位追加id部门本事【保留键值】，使用array_unshift（）后键值从新排序，故不使用
            $dataKeys = array_keys($groupdata);
            $dataValues= array_values($groupdata);
            #附加数据，并创建新的数组
            array_unshift($dataKeys,$self_info['depart_id']);
            array_unshift($dataValues,$self_info['title']);
            $groupdata = array_combine($dataKeys,$dataValues);
        }

        $this->view->assign('groupdata', $groupdata );

        //jstree节点数据(参数：根据部门ID查看当前部门归属)
        $nodeList = \app\admin\model\Department::getTreeList($this->auth->department_id);
        $this->assign("nodeList", $nodeList);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    public function index(){

        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        $pid = $this->request->get("pid", '', 'strip_tags');
        $this->view->assign("pid", isset($pid) && $pid ?$pid : 0);

        if ($this->request->isPost()) {

            $params = $this->request->post("row/a", [], 'strip_tags');

            $parentmodel = model("Department")->get($params['pid']);
            if (!$parentmodel) {
                $this->error(__('The parent group can not found'));
            }

            $params['level'] = $parentmodel['level']+1; //等级
            if ($params) {
                $this->model->create($params);
                $this->success();
            }
            $this->error();
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get(['depart_id' => $ids]);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {

            $params = $this->request->post("row/a", [], 'strip_tags');
            if ($params) {

                //判断是否是tree起点部门。不能更改上级
                if( !$this->auth->isSuperAdmin()){
                    if($this->auth->department_id == $params['pid']){
                        $this->error(__('无权编辑此节点'));
                    }
                }


                $result = $row->validate()->save($params);
                if ($result === false) {
                    $this->error($row->getError());
                }

                $this->success('编辑成功');
            }
            $this->error();
        }

        $this->view->assign("row", $row);
        $this->view->assign("ids", $ids);
        return $this->view->fetch();
    }
    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            if(!is_array($ids)){

                $ids = explode(',', $ids);
            }


            // 循环判断每一个组别是否可删除
            $grouplist = $this->model->where('depart_id', 'in', $ids)->select();
            foreach ($grouplist as $k => $v) {
                // 当前组别下有子组别
                $groupone = $this->model->get(['pid' => $v['id']]);
                if ($groupone) {
                    $ids = array_diff($ids, [$v['depart_id']]);
                    continue;
                }
            }
            if (!$ids) {
                $this->error(__('无法删除，存在下级部门'));
            }
            $count = $this->model->where('depart_id', 'in', $ids)->delete();
            if ($count) {
                $this->success();
            }
        }
        $this->error();
    }

    /**
     * 部门联动搜索
     */
    public function cxselect()
    {
        $level = $this->request->get('level');//部门登记
        $pid3 = $this->request->get('pid3');//上级部门
        $pid4 = $this->request->get('pid4');//上级部门
        $pid5 = $this->request->get('pid5');//上级部门

        $list = null;
        if($level==2){ //支队
            $pid = 0;
        }else if($level==3){ //大队
            $pid = $pid3;
        }else if($level==4){ //中队
            $pid = $pid4;
        }else if($level==5){ //班级
            $pid = $pid5;
        }

        $condition=[
            'pid'=>$pid,
            'level'=>$level
        ];

        $list = \app\admin\model\Department::where($condition )->field('depart_id as value, title AS name')->select();

        $this->success('', null, $list);
    }
}
