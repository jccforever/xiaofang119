<?php

namespace app\admin\controller\file;

use app\common\controller\Backend;

/**
 * 附件图片
 *
 * @icon fa fa-circle-o
 */
class File extends Backend
{
    
    /**
     * File模型对象
     * @var \app\admin\model\file\File
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\file\File;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();



            $total = $this->model
                ->where($where)
                ->where('category_id','IN',[4])
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where('category_id','IN',[4])
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $k=>$row) {

            }

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 添加
     */
    public function add()
    {


        if ($this->request->isPost()) {
            //$this->token();
            $params = $this->request->post("row/a", [], 'trim');

            if ($params) {

                $params['category_id'] = 4; //附件category_id
                $params['thumb'] = $this->params_data($params); //处理部门数据
                try {
                    $result = $this->model->create($params);
                } catch (Exception $e) {
                    $this->error($e->getMessage());
                }
                $this->success();

            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     * @param null $ids
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            //$this->token();
            $params = $this->request->post("row/a", [], 'trim');

            $params['category_id'] = 4; //附件category_id
            $params['thumb'] = $this->params_data($params); //处理部门数据
            try {
                $result = $row->allowField(true)->save($params);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error($row->getError());
                }
            } catch (\think\exception\PDOException $e) {
                $this->error($e->getMessage());
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }


    /**
     * 获取附件的小图标
     */
    public function params_data($data){
        $url = $data['url'] ;
        $host = "http://".$_SERVER['HTTP_HOST'] ;

        if(strpos($url,'.doc') !== false){
            $icon = "icon_doc.gif";
        }elseif(strpos($url,'.docx') !== false){
            $icon = "icon_doc.gif";
        }elseif(strpos($url,'.xlsx') !== false){
            $icon = "icon_xls.gif";
        }elseif(strpos($url,'.xls') !== false){
            $icon = "icon_xls.gif";
        }elseif(strpos($url,'.zip') !== false){
            $icon = "icon_rar.gif";
        }elseif(strpos($url,'.rar') !== false){
            $icon = "icon_rar.gif";
        }elseif(strpos($url,'.pdf') !== false){
            $icon = "icon_pdf.gif";
        }else{
            $icon = "icon_chm.gif";
        }
        $img = "<img src='".$host."/assets/addons/ueditor/dialogs/attachment/fileTypeImages/".$icon."'/>";
        $html ="<p style='line-height: 16px;'>".$img."<a style='font-size:12px; color:#0066cc;' href='".$host.$url."' title='". $data['title']."'>". $data['title']."</a></p>";
        return htmlspecialchars($html);
    }

}
