<?php

namespace app\admin\controller\user;

use app\admin\model\Department;
use app\common\controller\Backend;
use app\admin\model\user\Level as LevelModel;
use app\admin\model\Department as DepartModel;
use fast\Tree;

/**
 * 队员列管理
 *
 * @icon fa fa-circle-o
 */
class Member extends Backend
{
    
    /**
     * Member模型对象
     * @var \app\admin\model\user\Member
     */
    protected $model = null;
    protected $searchFields = 'user_id,realname,mobile';
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\user\Member;

        //职位数据
        $levelMode = new LevelModel();
        $this->view->assign('leveldata',$levelMode->getLevelData() );


        //部门列表(参数：权限+部门ID)
        $departTree = Department::getDepartList($this->auth->isSuperAdmin() , $this->auth->department_id,1);
        $this->view->assign('departTree', $departTree );


    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();


            $total = $this->model
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $DepartModel = model('Department');
            foreach ($list as $k=>$row) {
                $list[$k]['zd_name5'] = $DepartModel->getDepartName($row['zd_id5']);
                $list[$k]['zd_name4'] = $DepartModel->getDepartName($row['zd_id4']);
                $list[$k]['zd_name3'] = $DepartModel->getDepartName($row['zd_id3']);
                $list[$k]['zd_name2'] = $DepartModel->getDepartName($row['zd_id2']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 添加
     */
    public function add()
    {


        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a", [], 'trim');

            if ($params) {

                $params['create_time'] = time();
                $params = $this->params_data($params); //处理部门数据
                $params['join_time_stamp'] = strtotime($params['join_time']); //入伍时间戳
                try {

                    $result = $this->model->create($params);
                } catch (Exception $e) {
                    $this->error($e->getMessage());
                }
                $this->success();

            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     * @param null $ids
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a", [], 'trim');

            $params = $this->params_data($params);
            $params['join_time_stamp'] = strtotime($params['join_time']); //入伍时间戳

            try {
                $result = $row->allowField(true)->save($params);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error($row->getError());
                }
            } catch (\think\exception\PDOException $e) {
                $this->error($e->getMessage());
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $row['depart_id'] = $row['zd_id5']>0 ? $row['zd_id5'] : ( $row['zd_id4']>0 ? $row['zd_id4'] :( $row['zd_id3']>0 ? $row['zd_id3'] : $row['zd_id2'] )     ) ;
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    //处理队员参数
    function params_data($params){

        if(!validation_filter_id_card($params['card'])){
            return $this->error('请输入正确的身份证号码');
        }

        $card_info = getIDCardInfo($params['card']);
        $params['birthday'] = $card_info['birthday'];
        $params['age'] = $card_info['age'];
        $params['sex'] = $card_info['sex'];

        //部门
        $depart_id = $params['depart_id'] ;

        unset($params['depart_id']);
        $departModel = new DepartModel();
        $depart = $departModel::get($depart_id) ;

        $params['zd_id2'] =0;
        $params['zd_id3'] =0;
        $params['zd_id4'] =0;
        $params['zd_id5'] =0;
        if($depart['level'] == 5 ){
            $params['zd_id5'] = $depart_id ;
            $parent = $departModel->getParentDepart($depart_id) ;
            $params['zd_id4']  = $parent['depart_id']  ;

            $parent = $departModel->getParentDepart($params['zd_id4']) ;
            $params['zd_id3']  = $parent['depart_id']  ;

            $parent = $departModel->getParentDepart($params['zd_id3']) ;
            $params['zd_id2']  = $parent['depart_id']  ;
        }else if( $depart['level'] == 4 ){
            $params['zd_id4']  = $depart_id ;

            $parent = $departModel->getParentDepart($params['zd_id4']) ;
            $params['zd_id3']  = $parent['depart_id']  ;

            $parent = $departModel->getParentDepart($params['zd_id3']) ;
            $params['zd_id2']  = $parent['depart_id']  ;
        }else if( $depart['level'] == 3 ){
            $params['zd_id3']  = $depart_id ;

            $parent = $departModel->getParentDepart($params['zd_id3']) ;
            $params['zd_id2']  = $parent['depart_id']  ;
        }else if( $depart['level'] == 2 ){

            $parent = $departModel->getParentDepart($params['zd_id3']) ;
            $params['zd_id2']  = $depart_id ;
        }

        return $params;
    }

    //检测会员手机号唯一
    function checkMobile(){
        $params = $this->request->post("row/a", [], 'trim');
        $mobile = $params['mobile'] ;
        $type = $this->request->get('type');

        if($mobile ){
            $count = \app\admin\model\user\Member::where('mobile',$mobile)->count() ;
            if($type =='edit'){
                if($count>1){
                    $this->error('该手机号已被其他人注册。');
                }
            }else{
                if($count>0){
                    $this->error('该手机号已被其他人注册！');
                }
            }
            $this->success();
        }else{
            $this->success();
        }

    }


}
