<?php

namespace app\admin\controller\course;

use app\common\controller\Backend;
use app\admin\model\course\Lesson;
use think\Db;

/**
 * 评分标准
 *
 * @icon fa fa-circle-o
 */
class ScoreTpl extends Backend
{
    
    /**
     * ScoreTpl模型对象
     * @var \app\admin\model\course\ScoreTpl
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\course\ScoreTpl;

        //年龄段

        //年龄段
        $age1 =$this->age1= [-1,12,20,25,28,31,34,37,40,43,46,49,52];
        $age2 = $this->age2= [20,24,27,30,33,36,39,42,45,48,51];
        $this->view->assign('age1',$age1);
        $this->view->assign('age2',$age2);
        $this->assignconfig('age2', $age2) ;


    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();


            $total = $this->model
                ->where($where)
                ->order($sort, $order)
                ->count();

            $model = new Lesson();
            $list = $this->model
                ->where($where)

                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            foreach ($list as $k=>$row) {

                //年龄段
                if($row['age1'] == -1){
                    $list[$k]['age'] =  '入职者' ;
                }else if($row['age1'] == 12){
                    $list[$k]['age'] =  '20岁以下';
                }else if($row['age1'] == 52){
                    $list[$k]['age'] = '51岁以上';
                }else{
                    $list[$k]['age'] = $row['age1'].'~'.$row['age2'];
                }
                $list[$k]['lesson'] = $model->getInfo($row['pid']); //new \app\admin\model\course\Lesson()::getInfo($row['pid']);
            }

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list );

            return json($result);
        }


        return $this->view->fetch();
    }

    /**
     * 新增
     * @param null $ids
     */
    public function add($ids = null)
    {


        //表结构
        $tkist = Db::name('score_tpl')->query("show columns from fa_score_tpl");
        unset($tkist[0]);unset($tkist[1]);unset($tkist[2]);unset($tkist[3]);
        unset($tkist[4]);
        unset($tkist[5]);
        foreach($tkist as $k=>$val){
            $field = explode('_',$val['Field']);
            $tkist[$k]['score'] = $field[1];
        }


        if ($this->request->isPost()) {
            //$this->token();

            $post = $this->request->post("row/a", 0, 'trim');

            if(empty($post['age1']))
                $this->error('请选择年龄');

            if($post['unit'] ==2 || $post['unit'] ==3){

                $post['fen'] = $this->request->post("fen/a", [], 'trim');
                $post['miao'] = $this->request->post("miao/a", [], 'trim');
                $post['hm'] = $this->request->post("hm/a", [], 'trim');

                foreach($tkist as $k=>$val){
                    if( empty($post['fen'][$val['score']] )){ //分可以为空
                        $post['fen'][$val['score']] ='';
                    }
                    $post['fen'][$val['score']] =sprintf("%02d", $post['fen'][$val['score']]) ;

                    if( empty($post['miao'][$val['score']] )){
                        $post['miao'][$val['score']] ='00';
                    }
                    $post['miao'][$val['score']] =sprintf("%02d", $post['miao'][$val['score']]) ;

                    if( empty($post['hm'][$val['score']] )){
                        $post['hm'][$val['score']] ='00';
                    }
                    $post['hm'][$val['score']] =sprintf("%02d", $post['hm'][$val['score']]) ;

                    if($post['fen'][$val['score']] =='' ){
                        $post['s_'.$val['score']] = $post['miao'][$val['score']].'"'.$post['hm'][$val['score']] ;
                    }else{
                        $post['s_'.$val['score']] = $post['fen'][$val['score']].'′'.$post['miao'][$val['score']].'"'.$post['hm'][$val['score']] ;
                    }
                }
                unset($post['fen']);
                unset($post['miao']);
                unset($post['hm']);
            }

            //次数为空补0
            if($post['unit']==1){
                foreach($tkist as $k=>$val){
                    if( !$post[$tkist[$k]['Field']] ){
                        $post[$tkist[$k]['Field']] = 0;
                    }
                }
            }

            //年龄段
            if($post['age1'] == -1){
                $post['age2'] = -1 ;
            }else if($post['age1'] == 12){
                $post['age2'] =20;
            }else if($post['age1'] == 52){
                $post['age2'] =100;
            }

            //添加前的判断[主要是团队科目]
            if($post['type'] == 3){
                $CC = Db::name('score_tpl')->where('pid',$post['pid'])->count();
                if($CC > 0 ){
                    $this->error('团队科目只能存在一条标准');
                }
            }
            unset($post['type']);

            try {
                $result = $this->model->create($post);
            } catch (\think\exception\PDOException $e) {
                $this->error($e->getMessage());
            }
            $this->success();
            $this->error(__('Parameter %s can not be empty', ''));
        }

        //科目ID
        $lesson_id = $this->request->request('lesson_id') ;
        $this->view->assign('lesson_id',$lesson_id);
        //科目类型
        $lesson = Db::name('lesson')->where('id',$lesson_id)->find();
        $this->view->assign('lesson',$lesson);

        $this->view->assign('tkist',$tkist);

        return $this->view->fetch();
    }

    /**
     * 编辑
     * @param null $ids
     */
    public function edit($ids = null)
    {

        $row = $this->model->get($ids);
        //表结构
        $tkist = Db::name('score_tpl')->query("show columns from fa_score_tpl");
        unset($tkist[0]);unset($tkist[1]);unset($tkist[2]);unset($tkist[3]);
        unset($tkist[4]);
        unset($tkist[5]);
        foreach($tkist as $k=>$val){
            $field = explode('_',$val['Field']);
            $tkist[$k]['score'] = $field[1];
        }


        if ($this->request->isPost()) {
            //$this->token();


            $post = $this->request->post("row/a", 0, 'trim');

            if(empty($post['age1']))
                $this->error('请选择年龄');

            if($post['unit'] ==2 || $post['unit'] ==3){

                $post['fen'] = $this->request->post("fen/a", [], 'trim');
                $post['miao'] = $this->request->post("miao/a", [], 'trim');
                $post['hm'] = $this->request->post("hm/a", [], 'trim');

                foreach($tkist as $k=>$val){
                    if( empty($post['fen'][$val['score']] )){ //分可以为空
                        $post['fen'][$val['score']] ='';
                    }
                    $post['fen'][$val['score']] =sprintf("%02d", $post['fen'][$val['score']]) ;

                    if( empty($post['miao'][$val['score']] )){
                        $post['miao'][$val['score']] ='00';
                    }
                    $post['miao'][$val['score']] =sprintf("%02d", $post['miao'][$val['score']]) ;

                    if( empty($post['hm'][$val['score']] )){
                        $post['hm'][$val['score']] ='00';
                    }
                    $post['hm'][$val['score']] =sprintf("%02d", $post['hm'][$val['score']]) ;

                    if($post['fen'][$val['score']] =='' ){
                        $post['s_'.$val['score']] = $post['miao'][$val['score']].'"'.$post['hm'][$val['score']] ;
                    }else{
                        $post['s_'.$val['score']] = $post['fen'][$val['score']].'′'.$post['miao'][$val['score']].'"'.$post['hm'][$val['score']] ;
                    }
                }
                unset($post['fen']);
                unset($post['miao']);
                unset($post['hm']);
            }

            //次数为空补0
            if($post['unit']==1){
                foreach($tkist as $k=>$val){
                    if( !$post[$tkist[$k]['Field']] ){
                        $post[$tkist[$k]['Field']] = 0;
                    }
                }
            }

            //年龄段
            if($post['age1'] == -1){
                $post['age2'] = -1 ;
            }else if($post['age1'] == 12){
                $post['age2'] =20;
            }else if($post['age1'] == 52){
                $post['age2'] =100;
            }


            try {
                $result = $row->allowField(true)->save($post);
            } catch (\think\exception\PDOException $e) {
                $this->error($e->getMessage());
            }
            $this->success();
            $this->error(__('Parameter %s can not be empty', ''));
        }

        //评分详情

        $this->view->assign('row',$row);
        //拆分时间
        if($row['unit'] == 2 || $row['unit'] == 3){
                foreach($tkist as $kk=>$vv){
                    $temp=$temp2='';
                    $temp = $row['s_'.$vv['score']] ;
                    $temp = explode('′',$temp);
                    $tkist[$kk]['fen'] = $temp[0];
                    $temp2 =  explode('"',$temp[1]);
                    $tkist[$kk]['miao'] = $temp2[0];
                    $tkist[$kk]['hm'] = $temp2[1];
                }
        }else{
            foreach($tkist as $kk=>$vv){
                $tkist[$kk]['time'] = $row['s_'.$vv['score']] ;
            }
        }


        //科目ID
        $lesson_id = $row['pid'];
        $this->view->assign('lesson_id',$lesson_id);
        //科目类型
        $lesson = Db::name('lesson')->where('id',$lesson_id)->find();
        $this->view->assign('lesson',$lesson);

        $this->view->assign('tkist',$tkist);

        return $this->view->fetch();
    }

}
