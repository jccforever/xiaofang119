<?php

namespace app\admin\controller\course;

use app\common\controller\Backend;

use think\Db ;

/**
 * 评分标准
 *
 * @icon fa fa-circle-o
 */
class ScoreResult extends Backend
{
    
    /**
     * ScoreResult模型对象
     * @var \app\admin\model\course\ScoreResult
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\course\ScoreResult;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 编辑
     * @param null $ids
     */
    public function edit($ids = null)
    {

        
        if ($this->request->isPost()) {
            //$this->token();

            $score = $this->request->post("score/a", 0, 'trim');
            $pid = $this->request->post("pid", 0, 'trim');
            $unit = $this->request->post("unit", 0, 'trim');
            $type = $this->request->post("type", 0, 'trim');


            try {
                for($i=1;$i<5;$i++){

                        $condition=[
                            'pid'=>$pid,
                            'level'=>$i ,

                        ];
                        $logs = Db::name('score_result')->where($condition)->find();

                        if(!empty($logs)){
                            Db::name('score_result')->where('id',$logs['id'])->update(['score'=>$score[$i]]);
                        }else{
                            Db::name('score_result')->insert([
                                'score'=>$score[$i],
                                'pid'=>$pid,
                                'level'=>$i,
                                'unit'=>0
                            ]);
                        }
                    }

            } catch (\think\exception\PDOException $e) {
                $this->error($e->getMessage());
            }
            $this->success();
            $this->error(__('Parameter %s can not be empty', ''));
        }

        //评分等级
        $level = Db::name('score_level')->where(1)->order('id asc')->select();
        $this->view->assign('level',$level);

        //评分列表
        $result = $this->model->getResultList($ids);
        $this->view->assign('result',$result);

        //分数
        $array =[];
        for($i=100;$i>=0;$i-=5){
            $array[]=$i;
        }
        $this->view->assign('score',$array);

        //获取课程标题
        $model = new \app\admin\model\course\Lesson();
        $title = $model->getLessonTitle($ids);
        $this->view->assign('title',$title );
        $this->view->assign('pid',$ids );
        $this->view->assign('unit',Db::name('lesson')->where('id',$ids)->value('unit') );
        $this->view->assign('type',Db::name('lesson')->where('id',$ids)->value('type') );

        return $this->view->fetch();
    }

    /**
     * 编辑-质量评分标准
     * @param null $ids
     */
    public function edit3($ids = null)
    {


        if ($this->request->isPost()) {
            //$this->token();

            $score = $this->request->post("score/a", 0, 'trim');
            $pid = $this->request->post("pid", 0, 'trim');
            $unit = $this->request->post("unit", 0, 'trim');
            $type = $this->request->post("type", 0, 'trim');


            try {
                for($i=1;$i<5;$i++){
                    if($unit == 3){ //团队质量标准
                        $condition=[
                            'pid'=>$pid,
                            'level'=>$i ,
                            'unit'=>3,
                        ];
                        $logs = Db::name('score_result')->where( $condition)->find();
                        if(!empty($logs)){
                            Db::name('score_result')->where('id',$logs['id'])->update(['score'=>$score[$i]]);
                        }else{
                            Db::name('score_result')->insert([
                                'score'=>$score[$i],
                                'pid'=>$pid,
                                'level'=>$i,
                                'unit'=>3
                            ]);
                        }

                    }else{
                        $condition=[
                            'pid'=>$pid,
                            'level'=>$i ,

                        ];
                        $logs = Db::name('score_result')->where($condition)->find();

                        if(!empty($logs)){
                            Db::name('score_result')->where('id',$logs['id'])->update(['score'=>$score[$i]]);
                        }else{
                            Db::name('score_result')->insert([
                                'score'=>$score[$i],
                                'pid'=>$pid,
                                'level'=>$i,
                                'unit'=>0
                            ]);
                        }
                    }


                }
            } catch (\think\exception\PDOException $e) {
                $this->error($e->getMessage());
            }
            $this->success();
            $this->error(__('Parameter %s can not be empty', ''));
        }

        //评分等级
        $level = Db::name('score_level')->where(1)->order('id asc')->select();
        $this->view->assign('level',$level);

        //评分列表
        $result = $this->model->getResultList($ids ,3);
        $this->view->assign('result',$result);

        //分数
        $array =[];
        for($i=100;$i>=0;$i-=5){
            $array[]=$i;
        }
        $this->view->assign('score',$array);

        //获取课程标题
        $model = new \app\admin\model\course\Lesson();
        $title = $model->getLessonTitle($ids);
        $this->view->assign('title',$title );
        $this->view->assign('pid',$ids );
        $this->view->assign('unit',Db::name('lesson')->where('id',$ids)->value('unit') );
        $this->view->assign('type',Db::name('lesson')->where('id',$ids)->value('type') );
        return $this->view->fetch();
    }

}
