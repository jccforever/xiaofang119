<?php

namespace app\admin\controller\course;

use app\common\controller\Backend;

use think\Db;


/**
 * 科目列管理
 *
 * @icon fa fa-circle-o
 */
class Lesson extends Backend
{
    
    /**
     * Lesson模型对象
     * @var \app\admin\model\course\Lesson
     */
    protected $model = null;
    protected $searchFields = 'id,title';

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\course\Lesson;
        $this->view->assign("sexList", $this->model->getSexList());
        $this->view->assign("typeList", $this->model->getTypeList());
        $this->view->assign("unitList", $this->model->getUnitList());


    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            //url类别
            $param_type= $this->request->request('flag') ;
            $type_Data = explode('?',$param_type) ;
            $flag = $type_Data[0];

            //新增tab
            $TABwhere=[];
            $type = $this->request->request("type");
            if ($type == "all" || $type == null) {

            }else{
                if($flag ==1 || $flag ==2 ){
                    $TABwhere['sex'] = $type ;
                }
            }


            $total = $this->model
                ->where($where)
                ->where('type','=',$flag)
                ->where($TABwhere)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where('type','=',$flag)
                ->where($TABwhere)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            foreach ($list as $k=>$row) {
            }
//echo $this->model->getLastSql();
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list );

            return json($result);
        }


        $param_type= $this->request->request('flag') ;
        $type_Data = explode('?',$param_type) ;
        $flag = $type_Data[0];
        $this->view->assign('flag',$flag );

        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            //$this->token();
            $params = $this->request->post("row/a", [], 'trim');

            if ($params) {

                $params['create_time'] = time();

                $level = 0 ;
                if(isset($params['level'] )  && $params['level'] == 1 ){
                    $level = 1 ;
                }
                unset($params['level']);

                if($params['type'] <3 && $params['unit'] == 3){
                    $this->error('时间+质量标注只适用个人体能测试');
                }

                try {

                    $result = $this->model->create($params);
                    //评定标准【新增加有】
                    if($level == 1){
                        $list = Db::name('score_level')->order('id asc')->select();
                        foreach($list as $k=>$val){
                            Db::name('score_result')->insert(['pid'=>$result['id'] ,'score'=>$val['score'],'level'=>$val['id'] ]);
                        }
                    }

                } catch (Exception $e) {
                    $this->error($e->getMessage());
                }
                $this->success();

            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $param_type= $this->request->request('flag') ;
        $type_Data = explode('?',$param_type) ;
        $flag = $type_Data[0];
        $this->view->assign('flag',$flag );


        return $this->view->fetch();
    }


    /**
     * 编辑
     * @param null $ids
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            //$this->token();
            $params = $this->request->post("row/a", [], 'trim');

            try {
                $result = $row->allowField(true)->save($params);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error($row->getError());
                }
            } catch (\think\exception\PDOException $e) {
                $this->error($e->getMessage());
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $this->view->assign('row',$row);

        return $this->view->fetch();
    }
}
