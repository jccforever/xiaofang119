define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'report/logs/index' + location.search,
                    add_url: 'report/logs/add',
                    edit_url: 'report/logs/edit',
                    del_url: 'report/logs/del',
                    multi_url: 'report/logs/multi',
                    import_url: 'report/logs/import',
                    table: 'logs',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showToggle: false, //浏览模式
                showColumns: false,//显示隐藏列
                searchFormVisible: true,//搜索显示
                searchFormTemplate: 'customformtpl',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'user_id', title: __('User_id'), operate: 'LIKE'},
                        {field: 'lesson_id', title: __('Lesson_id'), operate: 'LIKE'},
                        {field: 'type', title: __('Type')},
                        {field: 'unit', title: __('Unit'), operate: 'LIKE'},
                        {field: 'num', title: __('Num')},
                        {field: 'fen', title: __('Fen')},
                        {field: 'miao', title: __('Miao')},
                        {field: 'hm', title: __('Hm')},
                        {field: 'score', title: __('Score'), operate: 'LIKE'},
                        {field: 'score_level', title: __('Score_level')},
                        {field: 'date', title: __('Date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'zd_id2', title: __('Zd_id2'), operate: 'LIKE'},
                        {field: 'zd_id3', title: __('Zd_id3'), operate: 'LIKE'},
                        {field: 'zd_id4', title: __('Zd_id4'), operate: 'LIKE'},
                        {field: 'zd_id5', title: __('Zd_id5'), operate: 'LIKE'},
                        {field: 'status', title: __('Status')},
                        {field: 'action_score', title: __('Action_score')},
                        {field: 'action_level', title: __('Action_level')},
                        {field: 'time_score', title: __('Time_score')},
                        {field: 'time_level', title: __('Time_level')},
                        {field: 'week', title: __('Week')},
                        {field: 'year', title: __('Year')},
                        {field: 'month', title: __('Month')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});