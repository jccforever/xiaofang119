define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    //读取选中的条目
    $.jstree.core.prototype.get_all_checked = function (full) {
        var obj = this.get_selected(), i, j;
        for (i = 0, j = obj.length; i < j; i++) {
            obj = obj.concat(this.get_node(obj[i]).parents);
        }
        obj = $.grep(obj, function (v, i, a) {
            return v != '#';
        });
        obj = obj.filter(function (itm, i, a) {
            return i == a.indexOf(itm);
        });
        return full ? $.map(obj, $.proxy(function (i) {
            return this.get_node(i);
        }, this)) : obj;
    };
    var Controller = {
        index: function () {

            $(document).on('click', '.btn-add', function () {
                    Fast.api.open("department/add", "新增部门", {
                        callback: function (data) {

                            Fast.api.ajax({
                                url: "department/add",
                                  data: {'row[pid]':data.pid ,'row[title]':data.title,'row[weigh]':data.weigh},
                            }, function (res, ret) {

                                if(ret.code){
                                    Toastr.success("操作成功");
                                    //刷新节点
                                    window.location.reload();
                                }else{
                                    Toastr.error("操作失败");
                                }
                            });
                        }
                    });
            });


            // 为表格绑定事件
            Controller.api.bindevent();
        },
        add: function () {

            $(document).on('click', '.btn-success', function () {
                let data=[];
                data['title'] = $("input[name='row[title]']").val()
                data['pid'] = $(".pid").val()
                data['weigh'] = $("input[name='row[weigh]']").val()
                Fast.api.close(data);
            });
        },
        edit: function () {
            $(document).on('click', '.btn-success', function () {
                let data=[];
                data['title'] = $("input[name='row[title]']").val()
                data['pid'] = $(".pid").val()
                data['weigh'] = $("input[name='row[weigh]']").val()
                Fast.api.close(data);
            });
        },
        api: {

            bindevent: function () {
                var ids=[];
                Form.api.bindevent($("form[role=form]"), null, null, function () {

                    return true;
                });
                //渲染权限节点树
                //销毁已有的节点树
                $("#treeview").jstree("destroy");
                Controller.api.rendertree(nodeData);
                //全选和展开
                $(document).on("click", "#checkall", function () {
                    $("#treeview").jstree($(this).prop("checked") ? "check_all" : "uncheck_all");
                });
                $(document).on("click", "#expandall", function () {

                    //展开全部
                    if( $(this).find("i").hasClass("fa-minus") ){

                        $(this).find("i").removeClass("fa-minus")
                        $(this).find("i").addClass('fa-plus');
                        $("#treeview").jstree("close_all");
                    }else{
                        console.log($(this).find("i").hasClass("fa-minus") );
                        $(this).find("i").removeClass("fa-plus")
                        $(this).find("i").addClass('fa-minus');
                        $("#treeview").jstree("open_all");
                    }


                });

                $(document).on('click', '.btn-del', function () {

                    Fast.api.ajax({
                        url: "department/del" ,
                        data: {'ids':ids},
                    }, function (res, ret) {

                        if(ret.code){
                            Toastr.success("删除成功");
                            //刷新节点
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);


                        }else{
                            Toastr.error("操作失败");
                        }
                        return false;
                    });

                });

                //checkbox 选中事件
                $('#treeview').on("check_node.jstree", function (node, data, event) {
                    var instance = $('#treeview').jstree(true);//获取jstree对象
                    if(instance.get_checked(false) ){
                        $('.btn-del').removeClass('disabled')
                        ids = instance.get_checked(false) ;
                    }else{
                        $('.btn-del').addClass('disabled')
                    }

                });


                //checkbox 取消选中事件
                $('#treeview').on("uncheck_node.jstree", function (node, data, event) {
                    var instance = $('#treeview').jstree(true);//获取jstree对
                    if(instance.get_checked(false).length !=0 ){
                        $('.btn-del').removeClass('disabled')
                        ids = instance.get_checked(false) ;
                    }else{
                        $('.btn-del').addClass('disabled')
                    }
                });


            },


            rendertree: function (content) {




                $("#treeview")
                    .on('redraw.jstree', function (e) {
                        $(".layer-footer").attr("domrefresh", Math.random());
                    })

                    .jstree({

                        "checkbox": {
                            "keep_selected_style": true,//是否默认选中
                            "three_state": false,//父子级别级联选择
                            "tie_selection": false,

                        },
                        "types": { },

                        "plugins": ["checkbox","drag","types","search", "themes", "state",'contextmenu'],

                        "core": {

                            'check_callback': true,//如果保留为false，则防止创建、重命名、删除、移动或复制等操作
                            "data": content  //data 数据配置:在这里可以传递HTML字符串或JSON数组
                        },
                        'contextmenu':{
                            'items' : {
                                'add':{
                                    'label':'新增部门',
                                    'action':function(obj){
                                        //reference获取当前选中节点的引用
                                        var inst = jQuery.jstree.reference(obj.reference);
                                        //通过get_node方法获取节点的信息，类似于实例对象
                                        var clickedNode = inst.get_node(obj.reference);
                                        //修改
                                        Fast.api.open("department/add?pid=" + clickedNode.id, "新增部门", {
                                            callback: function (data) {

                                                Fast.api.ajax({
                                                    url: "department/add?pid=" + clickedNode.id,
                                                    data: {'row[pid]':data.pid ,'row[title]':data.title,'row[weigh]':data.weigh},
                                                }, function (res, ret) {

                                                    if(ret.code){
                                                        Toastr.success("操作成功");
                                                        //刷新节点
                                                        window.location.reload();

                                                    }else{
                                                        Toastr.error("操作失败");
                                                    }
                                                    return false;
                                                });
                                            }
                                        });

                                    }
                                },
                                'rename':{
                                    'label':'编辑部门',
                                    'action':function(obj){
                                        var inst = jQuery.jstree.reference(obj.reference);
                                        var clickedNode = inst.get_node(obj.reference);
                                        //修改
                                        Fast.api.open("department/edit?ids=" + clickedNode.id, "编辑部门", {
                                            callback: function (data) {

                                                Fast.api.ajax({
                                                    url: "department/edit?ids=" + clickedNode.id,
                                                    data: {'row[pid]':data.pid ,'row[title]':data.title,'row[weigh]':data.weigh},
                                                }, function (res, ret) {

                                                    if(ret.code){
                                                        Toastr.success("操作成功");
                                                        //刷新节点
                                                        window.location.reload();

                                                    }else{
                                                        Toastr.error("操作失败");
                                                    }
                                                    return false;
                                                });
                                            }
                                        });
                                        return;



                                    }
                                },
                                'delete':{
                                    "label": "删除部门",
                                    'action':function(obj){
                                        var inst = jQuery.jstree.reference(obj.reference);
                                        var clickedNode = inst.get_node(obj.reference);


                                    }
                                }
                            }
                        },
                        'state': {
                            "opened":true, //默认展开
                        },

                    });
            }
        }
    };
    return Controller;
});