define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'file/file/index' + location.search,
                    add_url: 'file/file/add',
                    edit_url: 'file/file/edit',
                    del_url: 'file/file/del',
                    multi_url: 'file/file/multi',
                    import_url: 'file/file/import',
                    table: 'banner',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showToggle: false, //浏览模式
                showColumns: false,//显示隐藏列
                searchFormVisible: true,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false},
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                        {field: 'url', title: __('Url'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            UE.getEditor('c-content',{
                    toolbars: [[//工具条
                        'source', '|', 'attachment'
                    ]]
                }
            );
            Controller.api.bindevent();
        },
        edit: function () {
            UE.getEditor('c-content',{
                    toolbars: [[//工具条
                        'source', '|', 'attachment'
                    ]]
                }
            );
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});