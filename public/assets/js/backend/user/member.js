define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/member/index' + location.search,
                    add_url: 'user/member/add',
                    edit_url: 'user/member/edit',
                    del_url: 'user/member/del',
                    multi_url: 'user/member/multi',
                    import_url: 'user/member/import',
                    table: 'member',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'user_id',
                sortName: 'user_id',
                showToggle: false, //浏览模式
                showColumns: false,//显示隐藏列
                searchFormVisible: true,//搜索显示
                searchFormTemplate: 'customformtpl',

                columns: [
                    [
                        {checkbox: true},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'realname', title: __('Realname'), operate: 'LIKE'},
                        {field: 'mobile', title: __('Mobile'), operate: 'LIKE'},
                        //{field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'birthday', title: __('Birthday'), operate:false},
                        {field: 'age', title: __('Age') ,operate:false},
                        //{field: 'zd_name2', title: __('Zd_id2')},
                        {field: 'zd_name3', title: __('Zd_id3') ,operate:false},
                        {field: 'zd_name4', title: __('Zd_id4') ,operate:false},
                        {field: 'zd_name5', title: __('Zd_id5') ,operate:false},
                        {field: 'card', title: __('Card'), operate: 'LIKE'},

                        //{field: 'sn', title: __('Sn'), operate: 'LIKE'},
                        //{field: 'level', title: __('Level')},
                        //{field: 'photo', title: __('Photo'), operate: 'LIKE'},
                        {field: 'join_time', title: __('Join_time'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});