define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'plan/company/index' + location.search,
                    add_url: 'plan/company/add',
                    edit_url: 'plan/company/edit',
                    del_url: 'plan/company/del',
                    multi_url: 'plan/company/multi',
                    import_url: 'plan/company/import',
                    table: 'company',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showToggle: false,
                visible: false,
                pageSize: 20,
                pageList: [10, 20, 50],
                searchFormTemplate: 'customformtpl',
                columns: [
                    [
                        {checkbox: true},

                        {field: 'cid', title: __('Cid'),operate: false,},
                        {field: 'title', title: __('Title'), operate: 'LIKE',align:'left',},
                        {field: 'in_depart', title: __('Zd_name3'), operate: false,},
                        {field: 'address', title: __('Address'), align:'left'},
                        {field: 'unit', title: __('Unit'), operate: false,},
                        {field: 'up_depart', title: __('Department'),operate: false,},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            UE.getEditor('c-content',{
                    toolbars: [[//工具条
                        'source', //源代码
                        'cleardoc', //清空文档
                        'insertcode', //代码语言
                        'fontfamily', //字体
                        'fontsize', //字号
                        'paragraph', //段落格式
                        'customstyle', //自定义标题
                        '|',
                        'simpleupload', //单图上传
                        'insertimage', //多图上传
                        'attachment', //附件
                        'emotion', //表情
                        'time', //时间
                        'date', //日期
                        'map', //Baidu地图
                        'edittip ', //编辑提示
                        'autotypeset', //自动排版
                        'touppercase', //字母大写
                        'tolowercase', //字母小写
                        'background', //背景
                        'template', //模板
                        'scrawl', //涂鸦
                        'music', //音乐
                        'insertvideo', //视频
                        '|',
                        'anchor', //锚点
                        'undo', //撤销
                        'redo', //重做
                        'pagebreak', //分页
                        'bold', //加粗
                        'indent', //首行缩进
                        'snapscreen', //截图
                        'italic', //斜体
                        'underline', //下划线
                        'strikethrough', //删除线
                        'subscript', //下标
                        'fontborder', //字符边框
                        'superscript', //上标
                        'formatmatch', //格式刷
                        'blockquote', //引用
                        'pasteplain', //纯文本粘贴模式
                        'selectall', //全选
                        'print', //打印
                        'link', //超链接
                        'horizontal', //分隔线
                        'removeformat', //清除格式
                        'unlink', //取消链接
                        '|',
                        'insertrow', //前插入行
                        'insertcol', //前插入列
                        'mergeright', //右合并单元格
                        'mergedown', //下合并单元格
                        'deleterow', //删除行
                        'deletecol', //删除列
                        'splittorows', //拆分成行
                        'splittocols', //拆分成列
                        'splittocells', //完全拆分单元格
                        'deletecaption', //删除表格标题
                        'inserttitle', //插入标题
                        'mergecells', //合并多个单元格
                        'deletetable', //删除表格
                        'insertparagraphbeforetable', //"表格前插入行"
                        'edittable', //表格属性
                        'edittd', //单元格属性
                        'spechars', //特殊字符
                        'searchreplace', //查询替换
                        'justifyleft', //居左对齐
                        'justifyright', //居右对齐
                        'justifycenter', //居中对齐
                        'justifyjustify', //两端对齐
                        'forecolor', //字体颜色
                        'backcolor', //背景色
                        'insertorderedlist', //有序列表
                        'insertunorderedlist', //无序列表
                        'fullscreen', //全屏
                        'directionalityltr', //从左向右输入
                        'directionalityrtl', //从右向左输入
                        'rowspacingtop', //段前距
                        'rowspacingbottom', //段后距
                        'insertframe', //插入Iframe
                        'imagenone', //默认
                        'imageleft', //左浮动
                        'imageright', //右浮动
                        'imagecenter', //居中
                        'lineheight', //行间距
                        'inserttable', //插入表格
                        'charts', // 图表
                        'preview', //预览



                    ]], scaleEnabled:true//设置不自动调整高度
                }
            );
            Controller.api.bindevent();
        },
        edit: function () {
            UE.getEditor('c-content',{
                    toolbars: [[//工具条
                        'source', //源代码
                        'cleardoc', //清空文档
                        'insertcode', //代码语言
                        'fontfamily', //字体
                        'fontsize', //字号
                        'paragraph', //段落格式
                        'customstyle', //自定义标题
                        '|',
                        'simpleupload', //单图上传
                        'insertimage', //多图上传
                        'attachment', //附件
                        'emotion', //表情
                        'time', //时间
                        'date', //日期
                        'map', //Baidu地图
                        'edittip ', //编辑提示
                        'autotypeset', //自动排版
                        'touppercase', //字母大写
                        'tolowercase', //字母小写
                        'background', //背景
                        'template', //模板
                        'scrawl', //涂鸦
                        'music', //音乐
                        'insertvideo', //视频
                        '|',
                        'anchor', //锚点
                        'undo', //撤销
                        'redo', //重做
                        'pagebreak', //分页
                        'bold', //加粗
                        'indent', //首行缩进
                        'snapscreen', //截图
                        'italic', //斜体
                        'underline', //下划线
                        'strikethrough', //删除线
                        'subscript', //下标
                        'fontborder', //字符边框
                        'superscript', //上标
                        'formatmatch', //格式刷
                        'blockquote', //引用
                        'pasteplain', //纯文本粘贴模式
                        'selectall', //全选
                        'print', //打印
                        'link', //超链接
                        'horizontal', //分隔线
                        'removeformat', //清除格式
                        'unlink', //取消链接
                        '|',
                        'insertrow', //前插入行
                        'insertcol', //前插入列
                        'mergeright', //右合并单元格
                        'mergedown', //下合并单元格
                        'deleterow', //删除行
                        'deletecol', //删除列
                        'splittorows', //拆分成行
                        'splittocols', //拆分成列
                        'splittocells', //完全拆分单元格
                        'deletecaption', //删除表格标题
                        'inserttitle', //插入标题
                        'mergecells', //合并多个单元格
                        'deletetable', //删除表格
                        'insertparagraphbeforetable', //"表格前插入行"
                        'edittable', //表格属性
                        'edittd', //单元格属性
                        'spechars', //特殊字符
                        'searchreplace', //查询替换
                        'justifyleft', //居左对齐
                        'justifyright', //居右对齐
                        'justifycenter', //居中对齐
                        'justifyjustify', //两端对齐
                        'forecolor', //字体颜色
                        'backcolor', //背景色
                        'insertorderedlist', //有序列表
                        'insertunorderedlist', //无序列表
                        'fullscreen', //全屏
                        'directionalityltr', //从左向右输入
                        'directionalityrtl', //从右向左输入
                        'rowspacingtop', //段前距
                        'rowspacingbottom', //段后距
                        'insertframe', //插入Iframe
                        'imagenone', //默认
                        'imageleft', //左浮动
                        'imageright', //右浮动
                        'imagecenter', //居中
                        'lineheight', //行间距
                        'inserttable', //插入表格
                        'charts', // 图表
                        'preview', //预览



                    ]], scaleEnabled:true//设置不自动调整高度
                }
            );
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});