define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'course/lesson/index' + location.search,
                    add_url: 'course/lesson/add?flag='+flag,
                    edit_url: 'course/lesson/edit',
                    del_url: 'course/lesson/del',
                    multi_url: 'course/lesson/multi',
                    import_url: 'course/lesson/import',
                    table: 'lesson',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showToggle: false, //浏览模式
                showColumns: false,//显示隐藏列


                showExport: false,

                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id') , operate:false,},
                        {field: 'title', title: __('Title'), operate: 'LIKE',align:"left"},
                        //{field: 'create_time', title: __('Create_time'), operate:false, addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'sex', title: __('Sex'), searchList: {"1":__('Sex 1'),"2":__('Sex 2')},
                            formatter:function (value,row,index) {
                                if(row.type ==3){
                                    return '团队科目';
                                }else{
                                    if(row.sex ==1){
                                        return '男子科目';
                                    }else{
                                        return '女子科目';
                                    }
                                }
                            } },
                        {field: 'unit', title: __('Unit'), operate:false,searchList: {"1":__('Unit 1'),"2":__('Unit 2'),"3":__('Unit 3')}, formatter: Table.api.formatter.normal},

                        {
                            field: 'buttons',
                            width: "120px",
                            title: __('按钮组'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'detail',
                                    text: __('得分标准'),
                                    title: __('得分标准'),
                                    classname: 'btn btn-xs btn-success btn-dialog',
                                    icon: 'fa fa-list',
                                    extend:'data-area=\'["100%","100%"]\'',
                                    url: 'course/score_tpl/index/pid/{id}',
                                    callback: function (data) {
                                        //Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                }, {
                                    name: 'detail',
                                    text: __('评定标准'),
                                    title: __('评定标准'),
                                    classname: 'btn btn-xs btn-success btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'course/score_result/edit',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        return true;
                                        //返回true时按钮显示,返回false隐藏

                                    }
                                },
                                {
                                    name: 'detail',
                                    text: __('质量标准'),
                                    title: __('质量标准'),
                                    classname: 'btn btn-xs btn-success btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'course/score_result/edit3',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        if(row.type ==3 && row.unit == 3){
                                            return true;
                                        }else{
                                            return false;
                                        }
                                    }
                                }

                            ],
                            formatter: Table.api.formatter.buttons
                        },

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            // 为表格绑定事件
            Table.api.bindevent(table);

            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // var options = table.bootstrapTable(tableOptions);
                var typeStr = $(this).attr("href").replace('#', '');
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    // params.filter = JSON.stringify({type: typeStr});
                    params.type = typeStr;

                    return params;
                };
                table.bootstrapTable('refresh', {});
                return false;

            });
        },
        add: function () {
            var flag = Fast.api.query("flag") //类型
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});