define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'course/score_tpl/index' + location.search,
                    add_url: 'course/score_tpl/add?lesson_id='+Fast.api.query("pid") ,
                    edit_url: 'course/score_tpl/edit',
                    del_url: 'course/score_tpl/del',
                    multi_url: 'course/score_tpl/multi',
                    import_url: 'course/score_tpl/import',
                    table: 'score_tpl',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',

                visible: false,
                showToggle: false,
                showColumns: false,
                search:false,
                showExport: false,

                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id') ,operate:false},
                        {field: 'pid', title: __('Pid'),
                            formatter:function(value, row, index){
                                return row['lesson']['title'];
                            }
                        },
                        {field: 'age', title: __('Age') ,operate:false,},
                        // {field: 'age2', title: __('Age2')},

                        // {field: 'sort', title: __('Sort')},
                        // {field: 'unit', title: __('Unit')},
                        {field: 's_100', title: __('S_100'), operate:false,},
                        {field: 's_95', title: __('S_95'), operate:false,},
                        {field: 's_90', title: __('S_90'), operate:false,},
                        {field: 's_85', title: __('S_85'), operate:false,},
                        {field: 's_80', title: __('S_80'), operate:false,},
                        {field: 's_75', title: __('S_75'), operate:false,},
                        {field: 's_70', title: __('S_70'), operate:false,},
                        {field: 's_65', title: __('S_65'), operate:false,},
                        {field: 's_60', title: __('S_60'), operate:false,},
                        {field: 's_55', title: __('S_55'), operate:false,},
                        {field: 's_50', title: __('S_50'), operate:false,},
                        {field: 's_40', title: __('S_40'), operate:false,},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            var age2 = Config.age2 ;

            $('#c-age1').on('change', function (e) {
                var select=document.getElementsByName("row[age1]")[0];
                var key=select.selectedIndex; //下标

                var value = $(this).val();
                if(value == -1){
                    $('.hidden2').hide();
                }else if(value == 52){
                    $('.hidden2').hide();
                }else if(value >0 && value < 20){
                    $('.hidden2').hide();
                }else{
                    $('.hidden2').show();
                    $('#c-age2').val(age2[key-2]);

                }

            });
            Controller.api.bindevent();
        },
        edit: function () {
            var age2 = Config.age2 ;

            $('#c-age1').on('change', function (e) {
                var select=document.getElementsByName("row[age1]")[0];
                var key=select.selectedIndex; //下标

                var value = $(this).val();
                if(value == -1){
                    $('.hidden2').hide();
                }else if(value == 52){
                    $('.hidden2').hide();
                }else if(value >0 && value < 20){
                    $('.hidden2').hide();
                }else{
                    $('.hidden2').show();
                    $('#c-age2').val(age2[key-2]);

                }

            });
            Controller.api.bindevent();
        },

        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});