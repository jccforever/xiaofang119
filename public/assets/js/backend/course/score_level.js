define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'course/score_level/index' + location.search,
                    add_url: 'course/score_level/add',
                    edit_url: 'course/score_level/edit',
                    del_url: 'course/score_level/del',
                    multi_url: 'course/score_level/multi',
                    import_url: 'course/score_level/import',
                    table: 'score_level',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                sortOrder:'asc',//默认排序方式
                showToggle: false, //浏览模式
                showColumns: false,//显示隐藏列
                searchFormVisible: true,
                commonSearch: false,
                showExport: false,

                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'score', title: __('Score')},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        {field: 'operate', title: __('Operate'), table: table,events: Table.api.events.operate, formatter: function (value, row, index) {

                                var that = $.extend({}, this);
                                var table = $(that.table).clone(true);
// $(table).data("operate-edit", null);
                                $(table).data("operate-del", null);
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});