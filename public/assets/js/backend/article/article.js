define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'article/article/index' + location.search,
                    add_url: 'article/article/add',
                    edit_url: 'article/article/edit',
                    del_url: 'article/article/del',
                    multi_url: 'article/article/multi',
                    import_url: 'article/article/import',
                    table: 'web_article',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'article_id',
                sortName: 'article_id',
                showToggle: false, //浏览模式
                showColumns: false,//显示隐藏列
                //searchFormVisible: true,//搜索显示
                searchFormTemplate: 'customformtpl',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'article_id', title: __('Article_id')},
                        {field: 'title', title: __('Title'), operate: 'LIKE',align:'left'},
                        {field: 'category.name', title: __('Category_id')},
                        {field: 'admin.realname', title: __('Admin_id')},
                        {field: 'on_time', title: __('On_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            //分类下拉
            $(document).on("change", "#c-category_id", function(){
                var id = $('#c-category_id').val() ;//分类ID
                if(id == 2){
                    $('.key-show').show();
                }else{
                    $('.key-show').hide();
                }
            });
            UE.getEditor('c-content',{
                toolbars: [[//工具条
                    'source', '|',
                    'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', '|',
                    'rowspacingbottom', 'lineheight', '|',
                    'fontfamily', 'fontsize',
                    'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify',
                    'simpleupload', 'insertimage', 'attachment'



                ]]
            } );
            Controller.api.bindevent();
        },
        edit: function () {

            var id = $('#c-category_id').val() ;//分类ID
            console.log('分类' +id)
            if(id == 2){
                $('.key-show').show();
            }else{
                $('.key-show').hide();
            }

            //分类下拉
            $(document).on("change", "#c-category_id", function(){
                var id = $('#c-category_id').val() ;//分类ID
                if(id == 2){
                    $('.key-show').show();
                }else{
                    $('.key-show').hide();
                }
            });

            UE.getEditor('c-content',{
                toolbars: [[//工具条
                    'source', '|',
                    'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', '|',
                    'rowspacingbottom', 'lineheight', '|',
                    'fontfamily', 'fontsize',
                    'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify',
                    'simpleupload', 'insertimage', 'attachment'



                ]]
            } );

            Controller.api.bindevent();
        },
        api: {



            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});