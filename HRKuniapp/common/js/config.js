var config = {
	

	 
	 //本地环境
	  	baseUrl: 'http://127.0.0.6',
	  	 imgDomain: 'http://127.0.0.6',
	  	 h5Domain: 'http://localhost:8080/#',
	 	
	 	//正式环境
	 	baseUrl: 'http://hengkangruichuang.com',
	 	imgDomain: 'http://hengkangruichuang.com',
	 	h5Domain: 'http://hengkangruichuang.com/h5/#',


	  
	// 腾讯地图key
	mpKey: '', 
	//客服地址
	webSocket : '',
	//本地端主动给服务器ping的时间, 0 则不开启 , 单位秒
	pingInterval: 1500
};

export default config;