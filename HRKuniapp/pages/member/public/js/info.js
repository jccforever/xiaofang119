import validate from '@/common/js/validate.js';
export default {
	data() {
		return {
			registerConfig: {},
			indent: 'all',
			customNavTitle: "会员信息",
			memberInfo: {
				headimg: ''
			},
		
			formData: {
				userHeadImg: '',
				number: '', //账号
				nickName: '', //昵称
				sex: '', //性别
				realName: '', //真实姓名
				birthday: '', //生日
				currentPassword: '', //当前密码
				newPassword: '', //新密码
				confirmPassword: '', //确认密码
				mobile: '', //手机号
				mobileVercode: '', //手机验证码
				mobileDynacode: '', //手机动态验证吗
				mobileCodeText: "",
				username: '',
				card:'',
				member_id:'',
				currentPassword1: '', //当前密码
				newPassword1: '', //新密码
				confirmPassword1: '', //确认密码
			},
			memberInfoformData: {
				userHeadImg: '',
				number: '', //账号
				nickName: '', //昵称
				sex: '', //性别
				realName: '', //真实姓名
				birthday: '', //生日
				currentPassword: '', //当前密码
				newPassword: '', //新密码
				confirmPassword: '', //确认密码
				mobile: '', //手机号
				mobileVercode: '', //手机验证码
				mobileDynacode: '', //手机动态验证吗
				mobileCodeText: "",
				member_id:'',
				card:'',
				currentPassword1: '', //当前密码
				newPassword1: '', //新密码
				confirmPassword1: '', //确认密码
			},
			langList: [],
			langIndex: 0,
			seconds: 120,
			timer: null,
			isSend: false,
			captcha: {
				id: '',
				img: ''
			},
			isIphoneX: false,
			items: [{
					value: '0',
					name: '未知'
				},
				{
					value: '1',
					name: '男',
					checked: 'true'
				},
				{
					value: '2',
					name: '女'
				}
			],
			current: 0,
			memberConfig: {
				is_audit: 0,
				is_enable: 0
			},
			
			//上传证件
			action: this.$config.baseUrl + '/api/upload/cardUpload',
			// 预置上传列表
			fileList: [],
			// fileList: [{
			// 	url: 'http://pics.sc.chinaz.com/files/pic/pic9/201912/hpic1886.jpg',
			// 	error: false,
			// 	progress: 100
			// }],
			showUploadList: true,
			customBtn: false,
			autoUpload: true, //自动上传
			showProgress: true,
			deletable: true,
			customStyle: false,
			maxCount: 1, //最大上传数量
			lists: [], // 组件内部的文件列表
			
		};
	},
	onLoad(option) {
		this.$langConfig.refresh();
		//this.formData.mobileCodeText = this.$lang('findanimateCode');
		if (option.back) {
			this.back = option.back;
		}
		//this.getCaptcha();

		if (option.action) {
			this.indent = option.action;
			this.setNavbarTitle()
		}
		
		//this.getRegisterConfig();
		this.isIphoneX = this.$util.uniappIsIPhoneX()
	},
	onShow() {
	
		this.getInfo();
	
	},
	onHide() {
		
	},
	watch: {
		
	},
	computed: {
		startDate() {
			return this.getDate('start');
		},
		endDate() {
			return this.getDate('end');
		}
	},
	methods: {
		
		//自定义返回
		goback1(){
			this.$util.redirectTo('/pages/member/index');
		},
		
	
		
		reUpload() {
			this.$refs.uUpload.reUpload();
		},
		clear() {
			this.$refs.uUpload.clear();
		},
		autoUploadChange(index) {
			this.autoUpload = index == 0 ? true : false;
		},
		controlChange(index) {
			if(index == 0) {
				this.showProgress = true;
				this.deletable = true;
			} else {
				this.showProgress = false;
				this.deletable = false;
			}
		},
		maxCountChange(index) {
			this.maxCount = index == 0 ? 1 : index == 1 ? 2 : 4;
		},
		customStyleChange(index) {
			if (index == 0) {
				this.showUploadList = false;
				this.customBtn = true;
				
			} else {
				this.showUploadList = true;
				this.customBtn = false;
			}
		},
		upload() {

			this.$refs.uUpload.upload();
		},
		deleteItem(index) {
			this.$refs.uUpload.remove(index);
		},
		onOversize(file, lists) {
			// console.log('onOversize', file, lists);
		},
		onPreview(url, lists) {
			// console.log('onPreview', url, lists);
		},
		onRemove(index, lists) {
			// console.log('onRemove', index, lists);
		},
		onSuccess(data, index, lists) {
			console.log('onSuccess', data, index, lists);
		},
		onChange(res, index, lists) {
			// console.log('onChange', res, index, lists);
		},
		error(res, index, lists) {
			// console.log('onError', res, index, lists);
		},
		onProgress(res, index, lists) {
			 console.log('onProgress', res, index, lists);
		},
		onUploaded(lists) {
		 console.log('onUploaded', lists);
		},
		
		//编辑上传
		onListChange(lists) {
			console.log(lists)
			this.showProgress = true
		
			if(lists.length >1){
				lists.splice(1); 
			}
			this.lists = lists;
		},
		beforeRemove(index, lists) {
			return true;
		},
		onChooseFail(e) {
			console.log(e);
		},
		//以上是上传
		
		
		// 初始化语言
		initLang() {
			//获取语言列表
			this.langList = this.$langConfig.list();
			if (!uni.getStorageSync("lang")) {
				this.langIndex = 0;
			} else {
				for (let i = 0; i < this.langList.length; i++) {
					if (this.langList[i].value == uni.getStorageSync("lang")) {
						this.langIndex = i;
						break;
					}
				}
			}
		},
		setNavbarTitle(){
			let title = '个人资料';
			switch (this.indent) {
				case 'name':
					title = this.$lang('modifyNickname');
					break;
				case 'realName':
					title = this.$lang('realName');
					break;
				case 'sex':
					title = this.$lang('sex');
					break;
				case 'birthday':
					title = this.$lang('birthday');
					break;
				case 'password':
					title = this.$lang('password');
					break;
				case 'mobile':
					title = this.$lang('mobile');
					break;
				case 'card':
					title = '身份证';
					break;	
			}
			uni.setNavigationBarTitle({
			    title: title
			});
		},
		// 初始化用户信息
		getInfo() {
			var that =this
			this.$api.sendRequest({
				url: '/api/member/info',
				success: res => {
					if (res.code == 0) {
						this.memberInfo = res.data;
						this.memberInfoformData.userHeadImg = this.memberInfo.headimg;
						
						this.memberInfoformData.number = this.memberInfo.username; //账号
						this.memberInfoformData.nickName = this.memberInfo.nickname; //昵称
						this.memberInfoformData.card = this.memberInfo.card; //身份证
						this.memberInfoformData.mobile = this.memberInfo.mobile; //电话
						this.memberInfoformData.realName = this.memberInfo.realname ? this.memberInfo.realname : '请输入真实姓名'; //真实姓名
						this.memberInfoformData.sex = this.memberInfo.sex == 0 ? '未知' : this.memberInfo.sex == 1 ? '男' : '女'; //性别
						this.memberInfoformData.birthday = this.memberInfo.birthday ? this.$util.timeStampTurnTime(this.memberInfo.birthday,
							'YYYY-MM-DD') : '请选择生日'; //生日
							
							
						this.memberInfoformData.mobile = this.memberInfo.mobile; //手机号
						this.formData.member_id = this.memberInfo.member_id;
						this.formData.username = this.memberInfo.username; //用户名
						this.formData.nickName = this.memberInfo.nickname; //昵称
						this.formData.realName = this.memberInfo.realname; //真实姓名
						this.formData.card = this.memberInfo.card; 
						this.formData.mobile = this.memberInfo.mobile; 
						this.formData.sex = this.memberInfo.sex; //性别
						this.formData.birthday = this.memberInfo.birthday ? this.$util.timeStampTurnTime(this.memberInfo.birthday,
							'YYYY-MM-DD') : '请选择生日'; //生日
							
							//证件照
							let ia =[{'url':''}]
							
							ia[0]['url'] =this.$config.baseUrl +'/'+ this.memberInfo.card
							this.fileList = ia
							if(ia[0]['url']){
								that.showProgress = false;
							}
					
							
					}
				
					if (this.$refs.loadingCover) this.$refs.loadingCover.hide();
				},
				fail: res => {
					if (this.$refs.loadingCover) this.$refs.loadingCover.hide();
				}
			});
		},
		// 切换编辑项
		modifyInfo(action) {
			switch (action) {
				case 'cancellation':
					this.getCancelStatus();
				break;
				case 'language':
					let newArray = [];
					for (let i = 0; i < this.langList.length; i++) {
						newArray.push(this.langList[i].name)
					}
					uni.showActionSheet({
						itemList: newArray,
						success: function(res) {
							if (vm.langIndex != res.tapIndex) {
								vm.$langConfig.change(vm.langList[res.tapIndex].value)
							}
						}
					});
				break;
				default:
					this.$util.redirectTo('/pages/member/info_edit/info_edit', { action });
			}
		},
		getCancelStatus() {
			this.$api.sendRequest({
				url: '/membercancel/api/membercancel/info',
				success: res => {
					if (res.code >= 0) {
						if (res.data) {
							if (res.data.status == 0) {
								this.$util.redirectTo('/otherpages/member/cancelstatus/cancelstatus', {
									back: '/pages/member/info/info'
								});
							} else if (res.data.status == 1) {
								this.$util.redirectTo('/otherpages/member/cancelsuccess/cancelsuccess', {
									back: '/pages/member/info/info'
								});
							} else {
								this.$util.redirectTo('/otherpages/member/cancelrefuse/cancelrefuse', {
									back: '/pages/member/info/info'
								});
							}
						} else {
							this.$util.redirectTo('/otherpages/member/cancellation/cancellation', {
								back: '/pages/member/info/info'
							});
						}
					}
				}
			});
		},
		// 导航返回
		NavReturn() {
			uni.navigateBack({
				delta: 1
			})
		},
		// 获取验证码
		getCaptcha() {
			this.$api.sendRequest({
				url: '/api/captcha/captcha',
				data: {
					captcha_id: this.captcha.id
				},
				success: res => {
					if (res.code >= 0) {
						this.captcha = res.data;
						this.captcha.img = this.captcha.img.replace(/\r\n/g, '');
					}
				}
			});
		},
		// 退出登录
		logout() {
			uni.showModal({
				title: '提示',
				content: '确定要退出登录吗',
				success: (res) => {
					if (res.confirm) {
						
						//清空openid（防止自动登录）
						this.$api.sendRequest({
							url: '/api/member/out',
							data: {},
							success: res => {
								if (res.code >= 0) {
									uni.removeStorage({
										key: 'token',
										success: res => {
											uni.setStorageSync('loginLock', 1);
											//购物车数量
											uni.removeStorageSync('userInfo')
											this.$store.dispatch('getCartNumber').then((e) => {})
											this.$util.redirectTo('/pages/member/index/index');
										}
									});
								} 
							},
							fail: res => {
								
							}
						});
						
						
						
						
					}
				}
			});
		},
		headImage() {
			this.$util.redirectTo("/otherpages/member/modify_face/modify_face");
		},
		// 检测手机是否已绑定
		async testBinding(type) {
			var res = await this.checkMobile();
			return res;
		},
		//获取注销的配置信息
		getMemberConfig() {
			this.$api.sendRequest({
				url: '/membercancel/api/membercancel/config',
				success: res => {
					if (res.code >= 0) {
						this.memberConfig = res.data
					}
				}
			});

		},
		save(type) {
			switch (type) {
				case 'username':
					this.modifyUserName();
					break;
				case 'name':
					this.modifyNickName();
					break;
				case 'realName':
					this.modifyRealName();
					break;
				case 'card':
					this.modifyCard();
					break;	
				case 'sex':
					this.modifySex();
					break;
				case 'birthday':
					this.modifyBirthday();
					break;
				case 'password':
					this.modifyPassword();
					break;
				case 'pw2':
					this.modifyPassword2();
					break;	
				case 'mobile':
					this.modifyMobile();
					break;
			}
		},
		
		// ------------------------修改用户名------------------------------
		modifyUserName(){
			if (this.formData.username == this.memberInfo.username) {
				this.$util.showToast({
					title: this.$lang('alikeusername')
				});
				return;
			}
			var rule = [{
				name: 'username',
				checkType: 'required',
				errorMsg: this.$lang('noEmityUsername')
			}];
			if (!rule.length) return;
			var checkRes = validate.check(this.formData, rule);
			if (checkRes) {
				this.$api.sendRequest({
					url: '/api/member/modifyusername',
					data: {
						username: this.formData.username
					},
					success: res => {
						if (res.code == 0) {
							this.$util.showToast({
								title: this.$lang("updateSuccess")
							});
							this.NavReturn();
						} else {
							this.$util.showToast({
								title: res.message
							});
						}
					}
				});
			} else {
				this.$util.showToast({
					title: validate.error
				});
			}
		},	
		// ------------------------修改昵称------------------------------
		
		modifyNickName() {
			if (this.formData.nickName == this.memberInfo.nickname) {
				this.$util.showToast({
					title: this.$lang('alikeNickname')
				});
				return;
			}
			var rule = [{
				name: 'nickName',
				checkType: 'required',
				errorMsg: this.$lang('noEmityNickname')
			}];
			if (!rule.length) return;
			var checkRes = validate.check(this.formData, rule);
			if (checkRes) {
				this.$api.sendRequest({
					url: '/api/member/modifynickname',
					data: {
						nickname: this.formData.nickName
					},
					success: res => {
						if (res.code == 0) {
							this.$util.showToast({
								title: this.$lang("updateSuccess")
							});
							this.NavReturn();
						} else {
							this.$util.showToast({
								title: res.message
							});
						}
					}
				});
			} else {
				this.$util.showToast({
					title: validate.error
				});
			}
		},

		// ------------------------修改真实姓名------------------------------
		modifyRealName() {
			if (this.formData.realName == this.memberInfo.realname && this.memberInfo.realname) {
				this.$util.showToast({
					title: '与原真实姓名一致，无需修改'
				});
				return;
			}
			var rule = [{
				name: 'realName',
				checkType: 'required',
				errorMsg: '真实姓名不能为空'
			}];
			if (!rule.length) return;
			var checkRes = validate.check(this.formData, rule);
			if (checkRes) {
				this.$api.sendRequest({
					url: '/api/member/modifyrealname',
					data: {
						realname: this.formData.realName
					},
					success: res => {
						if (res.code == 0) {
							this.$util.showToast({
								title: this.$lang("updateSuccess")
							});
							this.NavReturn();
						} else {
							this.$util.showToast({
								title: res.message
							});
						}
					}
				});
			} else {
				this.$util.showToast({
					title: validate.error
				});
			}
		},
		
		//---------------------------修改身份证------------------------------
		modifyCard() {
		
			if (this.formData.card == this.memberInfo.card && this.memberInfo.card) {
				this.$util.showToast({
					title: '与原号码一致，无需修改'
				});
				return;
			}
			var rule = [{
				name: 'card',
				checkType: 'required',
				errorMsg: '号码不能为空'
			}];
			if (!rule.length) return;
			if( ! ( this.formData.card.length == 11 || this.formData.card.length == 18) ){
				this.$util.showToast({
					title: '请输入正确的身份证'
				});
				return;
			}
			
			var checkRes = validate.check(this.formData, rule);
			if (checkRes) {
				this.$api.sendRequest({
					url: '/api/member/modifycard',
					data: {
						card: this.formData.card
					},
					success: res => {
						if (res.code == 0) {
							this.$util.showToast({
								title: this.$lang("updateSuccess")
							});
							this.NavReturn();
						} else {
							this.$util.showToast({
								title: res.message
							});
						}
					}
				});
			} else {
				this.$util.showToast({
					title: validate.error
				});
			}
		},

		// ------------------------修改性别------------------------------
		radioChange: function(evt) {
			for (let i = 0; i < this.items.length; i++) {
				if (this.items[i].value === evt.target.value) {
					this.formData.sex = i;
					break;
				}
			}
		},

		modifySex() {
			this.$api.sendRequest({
				url: '/api/member/modifysex',
				data: {
					sex: this.formData.sex
				},
				success: res => {
					if (res.code == 0) {
						this.$util.showToast({
							title: this.$lang("updateSuccess")
						});
						this.NavReturn();
					} else {
						this.$util.showToast({
							title: res.message
						});
					}
				}
			});
		},

		// ------------------------修改生日------------------------------

		bindDateChange: function(e) {
			this.formData.birthday = e.target.value
		},

		getDate(type) {
			const date = new Date();
			let year = date.getFullYear();
			let month = date.getMonth() + 1;
			let day = date.getDate();

			if (type === 'start') {
				year = year - 60;
			} else if (type === 'end') {
				year = year + 2;
			}
			month = month > 9 ? month : '0' + month;;
			day = day > 9 ? day : '0' + day;
			return `${year}-${month}-${day}`;
		},

		modifyBirthday() {
			this.$api.sendRequest({
				url: '/api/member/modifybirthday',
				data: {
					birthday: this.$util.timeTurnTimeStamp(this.formData.birthday)
				},
				success: res => {
					if (res.code == 0) {
						this.$util.showToast({
							title: this.$lang("updateSuccess")
						});
						this.NavReturn();
					} else {
						this.$util.showToast({
							title: res.message
						});
					}
				}
			});
		},

		// ------------------------修改密码------------------------------
		/**
		 * 获取注册配置
		 */
		getRegisterConfig() {
			this.$api.sendRequest({
				url: '/api/register/config',
				success: res => {
					if (res.code >= 0) {
						this.registerConfig = res.data.value;
					}
				}
			});
		},
		modifyPassword() {
			if (this.memberInfo.password) {
				var rule = [{
						name: 'currentPassword',
						checkType: 'required',
						errorMsg: this.$lang("pleaseInputOldPassword")
					},
					{
						name: 'newPassword',
						checkType: 'required',
						errorMsg: this.$lang("pleaseInputNewPassword")
					}
				];
			} else {
				var rule = [{
						name: 'mobileVercode',
						checkType: 'required',
						errorMsg: this.$lang("confirmCodeInput")
					},
					{
						name: 'mobileDynacode',
						checkType: 'required',
						errorMsg: this.$lang("animateCodeInput")
					},
					{
						name: 'newPassword',
						checkType: 'required',
						errorMsg: this.$lang("pleaseInputNewPassword")
					}
				];
			}

			let regConfig = this.registerConfig;
			if (regConfig.pwd_len > 0) {
				rule.push({
					name: 'newPassword',
					checkType: 'lengthMin',
					checkRule: regConfig.pwd_len,
					errorMsg: '新密码长度不能小于' + regConfig.pwd_len + '位'
				});
			}
			if (regConfig.pwd_complexity) {
				let passwordErrorMsg = '密码需包含',
					reg = '';
				if (regConfig.pwd_complexity.indexOf('number') != -1) {
					reg += '(?=.*?[0-9])';
					passwordErrorMsg += '数字';
				}
				if (regConfig.pwd_complexity.indexOf('letter') != -1) {
					reg += '(?=.*?[a-z])';
					passwordErrorMsg += '、小写字母';
				}
				if (regConfig.pwd_complexity.indexOf('upper_case') != -1) {
					reg += '(?=.*?[A-Z])';
					passwordErrorMsg += '、大写字母';
				}
				if (regConfig.pwd_complexity.indexOf('symbol') != -1) {
					reg += '(?=.*?[#?!@$%^&*-])';
					passwordErrorMsg += '、特殊字符';
				}
				rule.push({
					name: 'newPassword',
					checkType: 'reg',
					checkRule: reg,
					errorMsg: passwordErrorMsg
				});
			}
			var checkRes = validate.check(this.formData, rule);
			if (checkRes) {
				if (this.formData.currentPassword == this.formData.newPassword) {
					this.$util.showToast({
						title: '新密码不能与原密码相同'
					});
					return;
				}
				if (this.formData.newPassword != this.formData.confirmPassword) {
					this.$util.showToast({
						title: '两次密码不一致'
					});
					return;
				}
				this.$api.sendRequest({
					url: '/api/member/modifypassword',
					data: {
						new_password: this.formData.newPassword,
						old_password: this.formData.currentPassword,
						code: this.formData.mobileDynacode,
						key: uni.getStorageSync("password_mobile_key"),
					},
					success: res => {
						if (res.code == 0) {
							this.$util.showToast({
								title: this.$lang('updateSuccess')
							});
						setTimeout(function() {
						       uni.navigateBack({
						       	delta: 1
						       })
						    }, 1000);
							uni.removeStorageSync('password_mobile_key');
						} else {
							this.$util.showToast({
								title: res.message
							});
						}
					}
				});
			} else {
				this.$util.showToast({
					title: validate.error
				});
			}
		},


	//修改支付密码
	modifyPassword2() {
		console.log('支付密码')
		if (this.formData.newPassword1 != this.formData.confirmPassword1) {
			this.$util.showToast({
				title: '两次密码不一致'
			});
			return;
		}
		if (this.formData.newPassword1.length !=6) {
			this.$util.showToast({
				title: '请输入6位支付密码'
			});
			return;
		}
		
			this.$api.sendRequest({
				url: '/api/member/modifypassword2',
				data: {
					new_password: this.formData.newPassword1,
					old_password: this.formData.currentPassword1,

				},
				success: res => {
					if (res.code == 0) {
						this.$util.showToast({
							title: this.$lang('updateSuccess')
						});
						setTimeout(function() {
						       uni.navigateBack({
						       	delta: 1
						       })
						    }, 1000);
						
					} else {
						this.$util.showToast({
							title: res.message
						});
					}
				}
			});
		
	},
	

		// ------------------------修改手机号------------------------------
		// 验证手机号
		vertifyMobile() {
			var rule = [{
				name: 'mobile',
				checkType: 'required',
				errorMsg: '请输入手机号'
			}, {
				name: 'mobile',
				checkType: 'phoneno',
				errorMsg: '请输入正确的手机号'
			}];
			var checkRes = validate.check(this.formData, rule);
			if (!checkRes) {
				this.$util.showToast({
					title: validate.error
				});
				return false;
			}
			return true;
		},
		// 检测手机号是否存在
		async checkMobile() {
			if (!this.vertifyMobile()) return;
			let res = await this.$api.sendRequest({
				url: '/api/member/checkmobile',
				data: {
					mobile: this.formData.mobile
				},
				async: false
			});
			if (res.code != 0) {
				this.$util.showToast({
					title: res.message
				});
				return false;
			}
			return true;
		},

		// 发送短信动态码
		async bindMoblieCode() {
			if (this.seconds != 120) return;
			var rule = [{
					name: 'mobile',
					checkType: 'phoneno',
					errorMsg: this.$lang("surePhoneNumber")
				},
				{
					name: 'mobileVercode',
					checkType: 'required',
					errorMsg: this.$lang("confirmCodeInput")
				},
			];

			var checkRes = validate.check(this.formData, rule);

			if (checkRes && !this.isSend) {
				this.isSend = true;
				this.$api.sendRequest({
					url: '/api/member/bindmobliecode',
					data: {
						mobile: this.formData.mobile,
						captcha_id: this.captcha.id,
						captcha_code: this.formData.mobileVercode
					},
					success: res => {
						let data = res.data;
						if (data.key) {
							if (this.seconds == 120 && this.timer == null) {
								this.timer = setInterval(() => {
									this.seconds--;
									this.formData.mobileCodeText = '已发送(' + this.seconds + 's)';
								}, 1000);
							}
							uni.setStorageSync('mobile_key', data.key);
						} else {
							this.$util.showToast({
								title: res.message
							});
							this.isSend = false;
						}
					},
					fail: res => {
						this.isSend = false;
						this.getCaptcha();
					}
				});
			} else {
				this.$util.showToast({
					title: validate.error ? validate.error : '请勿重复点击'
				});
			}
		},
		async modifyMobile() {
			console.log('修改手机');
			var mobileRule = [{
					name: 'mobile',
					checkType: 'phoneno',
					errorMsg: this.$lang("surePhoneNumber")
				},
				
			];
			var checkRes = validate.check(this.formData, mobileRule);

			if (checkRes) {
				if (this.formData.mobile == this.memberInfo.mobile) {
					this.$util.showToast({
						title: this.$lang("alikePhone")
					});
					return;
				}
			
				
				this.$api.sendRequest({
					url: '/api/member/modifymobile',
					data: {
						mobile: this.formData.mobile,
						captcha_id: this.captcha.id,
						captcha_code: this.formData.mobileVercode,
						code: this.formData.mobileDynacode,
						key: uni.getStorageSync("mobile_key"),
					},
					success: res => {
						if (res.code == 0) {
							this.$util.showToast({
								title: this.$lang("updateSuccess")
							});
							if (this.back) {
								this.$util.redirectTo('/otherpages/member/pay_password/pay_password', {
									'back': this.back
								}, 'redirectTo')
							} else {
								this.NavReturn();
							}
						} else {
							this.$util.showToast({
								title: res.message
							});
						}
					},
					fail: res => {
						this.isSend = false;
						this.getCaptcha();
					}
				});
			} else {
				this.$util.showToast({
					title: validate.error
				});
			}
		},
		/**
		 * 修改密码发送动态码
		 */
		passwordMoblieCode() {
			if (this.seconds != 120) return;

			if (this.formData.mobileVercode == '') {
				this.$util.showToast({
					title: this.$lang("confirmCodeInput")
				});
				return;
			}

			if (!this.isSend) {
				this.isSend = true;
				this.$api.sendRequest({
					url: '/api/member/pwdmobliecode',
					data: {
						captcha_id: this.captcha.id,
						captcha_code: this.formData.mobileVercode
					},
					success: res => {
						let data = res.data;
						if (data.key) {
							if (this.seconds == 120 && this.timer == null) {
								this.timer = setInterval(() => {
									this.seconds--;
									this.formData.mobileCodeText = '已发送(' + this.seconds + 's)';
								}, 1000);
							}
							uni.setStorageSync('password_mobile_key', data.key);
						} else {
							this.$util.showToast({
								title: res.message
							});
							this.isSend = false;
						}
					},
					fail: res => {
						this.isSend = false;
						this.getCaptcha();
					}
				});
			} else {
				this.$util.showToast({
					title: '请勿重复点击'
				});
			}
		},
		initFormData() {
			this.formData.currentPassword = '';
			this.formData.newPassword = '';
			this.formData.confirmPassword = '';
			this.formData.mobileVercode = '';
			this.formData.mobileDynacode = '';
			this.formData.mobile = '';
		}
	}
};
